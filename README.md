# reactjs-redux-starter-kit
[![Build Status](https://travis-ci.org/bobony/reactjs-redux-starter-kit.svg?branch=master)](https://travis-ci.org/bobony/reactjs-redux-starter-kit)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)
Reactjs redux starter kit is a very simple and easy to use redux starter kit for reactjs nothing more nothing less just what you want to getting started.
Reactjs redux starter kit uses **standard** as a jslinter. Please free to use other javascript linters.
# Installing and running the setup
1. **clone the repository:** https://github.com/bobony/reactjs-redux-starter-kit.git
2. cd into reactjs-redux-starter-kit (**$cd reactjs-redux-starter-kit**) where package.json is present
3. **$npm install**
4. After the successfull installation run **$npm run dev** to start the dev server on localhost:3000
5. For production build use **$npm run build**

# Example App
Coming Soon..

# Contribution:
If you have any issues please report.

Suggestions and features are always welcome :)
