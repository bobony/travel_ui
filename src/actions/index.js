import axios from 'axios'
import {
FETCH_PLACES,
FETCH_PLACES_ERROR
} from './types'

const ROOT_URL = 'http://desalsa.io:3000'

export function fetchPlaces () {
  const config = {
    headers: {
      'Accept': 'application/json'
    }
  }
  return function (dispatch) {
    axios.get(ROOT_URL + '/api/app/', config)
.then(response => {
  dispatch({
    type: FETCH_PLACES,
    payload: response.data
  })
})
.catch(response => dispatch(fetchPlacesError(response)))
  }
}

export function fetchPlacesError (error) {
  return {
    type: FETCH_PLACES_ERROR,
    payload: error
  }
}
