import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../actions'
require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css')

class Places extends Component {
  componentWillMount () {
    this.props.fetchPlaces()
  }
  componentWillReceiveProps (nextProps) {
  }
  render () {
    const { places } = this.props
    console.log('I AM PLACES', places)

    return (
      <div>
        { places.map(function (place, index) {
          return (
            <div key={index}>
              <Place {...place} />
            </div>
          ) })}
      </div>
      )
  }
}
function mapStateToProps (state) {
  console.log('I AM STATETOPTOPS', state.places)
  return { places: state.places.data || []
          }
}
class Place extends Component {

  render () {
    console.log('I AM PROPS', this.props)
    const place = this.props
    return (
      <div>
        <p>{place.name}</p>
        <p>{place.description}</p>
      </div>
)
  }
}
export default connect(mapStateToProps, actions)(Places)
