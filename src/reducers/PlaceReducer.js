import {
FETCH_PLACES,
FETCH_PLACES_ERROR
} from '../actions/types'

export default function (state = {}, action) {
  switch (action.type) {
    case FETCH_PLACES:
      // console.log('I AM STATE...', ...state, state, action.payload)
      return {...state, data: action.payload}
    case FETCH_PLACES_ERROR:
      return {...state, error: action.payload}
    default:
       // console.log('I AM STATE', state)
      return state
  }
  // return state
}
