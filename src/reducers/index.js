import { combineReducers } from 'redux'
import places from './PlaceReducer'
const rootReducer = combineReducers({
  places
})
export default rootReducer
