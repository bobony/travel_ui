import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import reduxThunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import App from './components/App'
import reducers from './reducers'
// import Home from './components/home/Home'
import Places from './components/places/Places'
import NotFound from './components/misc/NotFound'
const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore)
const store = createStoreWithMiddleware(reducers)

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path='/' component={App}>
        <IndexRoute component={Places} />
        <Route path='*' component={NotFound} />
      </Route>
      <Route path='place' component={Places} />
    </Router>
  </Provider>
  , document.querySelector('.container'))
